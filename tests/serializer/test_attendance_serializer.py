import pytest

from huscy.participations.models import Attendance
from huscy.participations.serializer import AttendanceSerializer

pytestmark = pytest.mark.django_db


def test_attendance_serializer(attendance):
    data = AttendanceSerializer(instance=attendance).data

    assert data == dict(
        end='2000-12-24T15:00:00',
        # planned_end='2000-12-24T11:00:00',
        # booking.Timeslot @property def end incorrect
        planned_start='2000-12-24T10:00:00',
        session=attendance.booking.timeslot.session.id,
        start='2000-12-24T10:00:00',
        status=Attendance.STATUS.scheduled,
        status_display='Scheduled',
    ), (data)
