import pytest

from huscy.participations.models import Participation
from huscy.participations.serializer import ParticipationSerializer, AttendanceSerializer

pytestmark = pytest.mark.django_db


def test_with_declined_participation(declined_participation, subject):
    data = ParticipationSerializer(instance=declined_participation).data

    assert data == dict(
        attendances=[],
        status=Participation.STATUS.declined,
        status_display='Declined',
        experiment=declined_participation.experiment.id,
        experiment_title=declined_participation.experiment.title,
        subject=subject.id,
        subject_display_name='display_name',
    )


def test_with_pending_participation(pending_participation, subject):
    data = ParticipationSerializer(instance=pending_participation).data

    assert data == dict(
        attendances=AttendanceSerializer(
            instance=pending_participation.attendance_set.all(), many=True
        ).data,
        status=Participation.STATUS.pending,
        status_display='Pending',
        experiment=pending_participation.experiment.id,
        experiment_title=pending_participation.experiment.title,
        subject=subject.id,
        subject_display_name='display_name',
    )
