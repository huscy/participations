import pytest
from rest_framework.reverse import reverse
from rest_framework.status import HTTP_200_OK, HTTP_201_CREATED, HTTP_403_FORBIDDEN

from huscy.participations.models import Participation


pytestmark = pytest.mark.skip


def test_admin_user_can_create_participation(admin_client, project, experiment, subject):
    response = create_participation(admin_client, project, experiment, subject)

    assert response.status_code == HTTP_201_CREATED, response.json()


def test_admin_user_can_list_experiment_participations(admin_client, project, experiment):
    response = list_experiment_participations(admin_client, project, experiment)

    assert response.status_code == HTTP_200_OK


"""
def test_admin_user_can_list_subject_group_participations(admin_client, project, experiment,
                                                          subject_group):
    response = list_subject_group_participations(admin_client, project, experiment, subject_group)

    assert response.status_code == HTTP_200_OK
"""


def test_user_without_permissions_can_create_participation(client, experiment, subject):
    response = create_participation(client, experiment.project, experiment, subject)

    assert response.status_code == HTTP_201_CREATED, response.json()


def test_user_without_permissions_can_list_experiment_participations(client, project, experiment):
    response = list_experiment_participations(client, project, experiment)

    assert response.status_code == HTTP_200_OK


"""
def test_user_without_permissions_can_list_subject_group_participations(client,
project, experiment, subject_group):
    response = list_subject_group_participations(client, project, experiment, subject_group)

    assert response.status_code == HTTP_200_OK
"""


def test_anonymous_user_cannot_create_participation(anonymous_client, experiment, subject):
    response = create_participation(anonymous_client, experiment.project, experiment, subject)

    assert response.status_code == HTTP_403_FORBIDDEN


def test_anonymous_user_cannot_list_experiment_participations(anonymous_client, project,
                                                              experiment):
    response = list_experiment_participations(anonymous_client, project, experiment)

    assert response.status_code == HTTP_403_FORBIDDEN


"""
def test_anonymous_user_cannot_list_subject_group_participations(anonymous_client, project,
                                                                 experiment, subject_group):
    response = list_subject_group_participations(anonymous_client, project, experiment,
                                                 subject_group)

    assert response.status_code == HTTP_403_FORBIDDEN
"""


def test_create_declined_participation(client, project, experiment, subject):
    response = create_participation(client, project, experiment, subject)

    participation = response.json()
    print(f"Dict: {participation}")
    assert participation['status'] == Participation.STATUS.declined
    assert participation['status_display'] == 'Declined'


"""
def test_create_pending_participation(client, project, experiment, subject, timeslots):
    response = create_participation(client, project, experiment, subject, timeslots)

    participation = response.json()
    assert participation['status'] == Participation.STATUS.get_value('pending')
    assert participation['status_display'] == 'Pending'
"""


def create_participation(client, project, experiment, subject, timeslots=None):
    data = {'subject': subject.id}
    if timeslots:
        data['timeslots'] = [timeslot.id for timeslot in timeslots]

    return client.post(
        reverse('experiment-participation-list',
                kwargs=dict(project_pk=project.pk, experiment_pk=experiment.id)),
        data=data,
    )


def list_experiment_participations(client, project, experiment):
    return client.get(
        reverse('experiment-participation-list',
                kwargs=dict(project_pk=project.pk, experiment_pk=experiment.id)))


"""
def list_subject_group_participations(client, project, experiment, subject_group):
    return client.get(reverse('subjectgroup-participation-list',
                              kwargs=dict(project_pk=project.pk, experiment_pk=experiment.id,
                                          subjectgroup_pk=subject_group.id)))
"""
