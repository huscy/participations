from datetime import datetime  # timedelta
from itertools import cycle

import pytest
from model_bakery import baker

from rest_framework.test import APIClient

from huscy.participations.models import Attendance, Participation
from huscy.pseudonyms.services import create_pseudonym


@pytest.fixture
def user(django_user_model):
    return django_user_model.objects.create_user(username='user', password='password',
                                                 first_name='John', last_name='Gleur')


@pytest.fixture
def admin_client(admin_user):
    client = APIClient()
    client.login(username=admin_user.username, password='password')
    return client


@pytest.fixture
def client(user):
    client = APIClient()
    client.login(username=user.username, password='password')
    return client


@pytest.fixture
def anonymous_client(user):
    return APIClient()


@pytest.fixture
def project():
    return baker.make('projects.Project')


@pytest.fixture
def experiment(project):
    return baker.make('project_design.Experiment', project=project)


@pytest.fixture
def sessions(experiment):
    return baker.make('project_design.Session', experiment=experiment,
                      # duration=timedelta(hours=1),
                      _quantity=2)


@pytest.fixture
def timeslots(sessions):
    return [
        baker.make('bookings.Timeslot', start=datetime(2000, 12, 24, 10), session=sessions[0]),
        baker.make('bookings.Timeslot', start=datetime(2000, 12, 26, 10), session=sessions[1]),
    ]


@pytest.fixture
def bookings(timeslots):
    return baker.make('bookings.Booking', timeslot=cycle(timeslots), _quantity=2)


"""
@pytest.fixture
def subject_group(experiment):
    return baker.make('recruitment.SubjectGroup', experiment=experiment,
                      name='subject_group_name')


@pytest.fixture
def attribute_filterset(subject_group):
    return baker.make('recruitment.AttributeFilterSet', subject_group=subject_group)
"""


@pytest.fixture
def subject():
    return baker.make('subjects.Subject', contact__display_name='display_name')


@pytest.fixture
def pseudonym(experiment, subject):
    return create_pseudonym(subject, 'participations.participation', object_id=experiment.id)


@pytest.fixture
def declined_participation(experiment, pseudonym):
    return baker.make('participations.Participation', pseudonym=pseudonym.code,
                      experiment=experiment)


@pytest.fixture
def pending_participation(experiment, pseudonym, bookings):
    participation = baker.make('participations.Participation', pseudonym=pseudonym.code,
                               experiment=experiment,
                               status=Participation.STATUS.pending)
    baker.make('participations.Attendance', participation=participation, booking=cycle(bookings),
               _quantity=2)
    return participation


@pytest.fixture
def attendance(bookings):
    return baker.make('participations.Attendance', booking=bookings[0],
                      status=Attendance.STATUS.scheduled,
                      start=datetime(2000, 12, 24, 10), end=datetime(2000, 12, 24, 15))
