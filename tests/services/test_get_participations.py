"""
import pytest
from model_bakery import baker

from huscy.participations.services import get_participations

pytestmark = pytest.mark.django_db


@pytest.fixture
def subject_group_2(experiment):
    return baker.make('recruitment.SubjectGroup', experiment=experiment)


@pytest.fixture
def subject_group_3():
    return baker.make('recruitment.SubjectGroup')


@pytest.fixture
def participations_subject_group_1(subject_group):
    return baker.make('participations.Participation', subject_group=subject_group, _quantity=3)


@pytest.fixture
def participations_subject_group_2(subject_group_2):
    return baker.make('participations.Participation', subject_group=subject_group_2, _quantity=3)


@pytest.fixture
def participations(participations_subject_group_1, participations_subject_group_2):
    return participations_subject_group_1 + participations_subject_group_2


def test_get_participations(experiment, participations):
    result = get_participations(experiment)

    assert list(result) == participations


def test_get_participations_for_subject_group(experiment,
                                              subject_group, subject_group_2, subject_group_3,
                                              participations_subject_group_1,
                                              participations_subject_group_2):

    assert participations_subject_group_1 == list(get_participations(experiment, subject_group))
    assert participations_subject_group_2 == list(get_participations(experiment, subject_group_2))
    assert [] == list(get_participations(experiment, subject_group_3))
"""
