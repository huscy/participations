"""
from unittest.mock import patch

import pytest

from huscy.bookings.models import Booking
from huscy.participations.models import Participation
from huscy.participations.services import create_participation
from huscy.recruitment.models import ParticipationRequest

pytestmark = pytest.mark.django_db

invited = ParticipationRequest.STATUS.get_value('invited')


def test_create_declined_participation(subject_group, attribute_filterset, subject):
    participation = create_participation(subject_group, subject)

    assert participation.subject_group == subject_group
    assert participation.status == Participation.STATUS.get_value('declined')


def test_create_pending_participation(subject_group, attribute_filterset, subject, timeslots):
    assert not ParticipationRequest.objects.exists()

    participation = create_participation(subject_group, subject, timeslots)

    assert participation.subject_group == subject_group
    assert participation.status == Participation.STATUS.get_value('pending')

    assert ParticipationRequest.objects.filter(status=invited).exists()


def test_rollback(subject_group, attribute_filterset, subject, timeslots):
    with patch('huscy.participations.services.create_or_update_participation_request',
               side_effect=Exception()):
        with pytest.raises(Exception):
            create_participation(subject_group, subject, timeslots)

    assert not Participation.objects.exists()
    assert not Booking.objects.exists()
"""
